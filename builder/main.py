# Copyright 2014-present PlatformIO <contact@platformio.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
from platform import system
from os import makedirs
from os.path import basename, isdir, join
import serial.tools.list_ports

from SCons.Script import (ARGUMENTS, COMMAND_LINE_TARGETS, AlwaysBuild,
                          Builder, Default, DefaultEnvironment)


def BeforeUploadSAMBA(target, source, env):  # pylint: disable=W0613,W0621

    upload_options = {}
    if "BOARD" in env:
        upload_options = env.BoardConfig().get("upload", {})
        
    serial_hwids = upload_options.get("serial_hwids", [["0x403","0x6015"]])
    matched_ser_ports = []
    
    for port in serial.tools.list_ports.comports():
        for hwid in serial_hwids:
            hwid_str = ("%s:%s" % (hwid[0], hwid[1])).replace("0x", "")
            if hwid_str in port.hwid:
                matched_ser_ports.append(port)
                
    if len(matched_ser_ports) == 0:
        sys.stderr.write("Couldn't find upload port")
        env.Exit(1)

    env.Replace(UPLOAD_PORT=matched_ser_ports[0].device)
    print(env.subst("Auto-detected: $UPLOAD_PORT"))
    # use only port name for BOSSA
    env.FlushSerialBuffer("$UPLOAD_PORT")
    env.Replace(UPLOAD_PORT=basename(env.subst("$UPLOAD_PORT")))
        


env = DefaultEnvironment()
platform = env.PioPlatform()
board = env.BoardConfig()
upload_protocol = env.subst("$UPLOAD_PROTOCOL")
build_mcu = env.get("BOARD_MCU", board.get("build.mcu", ""))

env.Replace(
    AR="arm-none-eabi-ar",
    AS="arm-none-eabi-as",
    CC="arm-none-eabi-gcc",
    CXX="arm-none-eabi-g++",
    GDB="arm-none-eabi-gdb",
    OBJCOPY="arm-none-eabi-objcopy",
    RANLIB="arm-none-eabi-ranlib",
    SIZETOOL="arm-none-eabi-size",

    ARFLAGS=["rc"],

    SIZEPROGREGEXP=r"^(?:\.text|\.data|\.rodata|\.text.align|\.ARM.exidx)\s+(\d+).*",
    SIZEDATAREGEXP=r"^(?:\.data|\.bss|\.noinit)\s+(\d+).*",
    SIZECHECKCMD="$SIZETOOL -A -d $SOURCES",
    SIZEPRINTCMD='$SIZETOOL -B -d $SOURCES',

    PROGSUFFIX=".elf"
)

# Allow user to override via pre:script
if env.get("PROGNAME", "program") == "program":
    env.Replace(PROGNAME="firmware")

env.Append(
    BUILDERS=dict(
        ElfToBin=Builder(
            action=env.VerboseAction(" ".join([
                "$OBJCOPY",
                "-O",
                "binary",
                "$SOURCES",
                "$TARGET"
            ]), "Building $TARGET"),
            suffix=".bin"
        ),
        ElfToHex=Builder(
            action=env.VerboseAction(" ".join([
                "$OBJCOPY",
                "-O",
                "ihex",
                "-R",
                ".eeprom",
                "$SOURCES",
                "$TARGET"
            ]), "Building $TARGET"),
            suffix=".hex"
        )
    )
)

if not env.get("PIOFRAMEWORK"):
    env.SConscript("frameworks/_bare.py")

#
# Target: Build executable and linkable firmware
#

target_elf = None
if "nobuild" in COMMAND_LINE_TARGETS:
    target_firm = join("$BUILD_DIR", "${PROGNAME}.bin")
else:
    target_elf = env.BuildProgram()
    target_firm = env.ElfToBin(join("$BUILD_DIR", "${PROGNAME}"), target_elf)

AlwaysBuild(env.Alias("nobuild", target_firm))
target_buildprog = env.Alias("buildprog", target_firm, target_firm)

#
# Target: Print binary size
#

target_size = env.Alias("size", target_elf, env.VerboseAction("$SIZEPRINTCMD", "Calculating size $SOURCE"))
AlwaysBuild(target_size)

#
# Target: Upload by default .bin file
#

debug_tools = board.get("debug.tools", {})
upload_actions = []


if upload_protocol == "sam-ba":
    
    platform_name = system().lower()
    upload_tool_name = "bossac-bcb"
    if platform_name == "linux":
        upload_tool_name += "-linux"
    elif platform_name == "windows":
        upload_tool_name += "-windows.exe"
    elif platform_name == "darwin":
        upload_tool_name += "-darwin"
    else:
        sys.stderr.write("Warning! Unknown upload tool\n")
        
    env.Replace(UPLOADER=upload_tool_name,
                UPLOADERFLAGS=["--port", '"$UPLOAD_PORT"',
                                "--baudrate", "921600",
                                "--bcb-reset", # BCB reset using external co-processor
                                "--bcb-boot",
                                "--usb-port=0",
                                "--offset", board.get("upload.section_start", "0x2000"),
                                "--erase",
                                "--write",
                                "--verify"],
                UPLOADCMD="$UPLOADER $UPLOADERFLAGS $SOURCES")
                
    if int(ARGUMENTS.get("PIOVERBOSE", 0)):
        env.Prepend(UPLOADERFLAGS=["--info", "--debug"])

    upload_actions = [env.VerboseAction(BeforeUploadSAMBA, "Looking for upload port..."),
                        env.VerboseAction("$UPLOADCMD", "Uploading $SOURCE")]

elif upload_protocol in debug_tools:
    env.Replace(UPLOADER="openocd", 
                UPLOADERFLAGS=debug_tools.get(upload_protocol).get("server").get("arguments", []) 
                                + ["-c", 
                                    "program {{$SOURCE}} verify reset %s; shutdown" % board.get("upload.section_start", "")],
                UPLOADCMD="$UPLOADER $UPLOADERFLAGS")
    env['UPLOADERFLAGS'] = [f.replace("$PACKAGE_DIR", platform.get_package_dir("tool-openocd") or "") for f in env['UPLOADERFLAGS']]
    upload_actions = [env.VerboseAction("$UPLOADCMD", "Uploading $SOURCE")]

else:
    sys.stderr.write("Warning! Unknown upload protocol %s\n" % upload_protocol)

AlwaysBuild(env.Alias("upload", target_firm, upload_actions))

#
# Setup default targets
#

Default([target_buildprog, target_size])
